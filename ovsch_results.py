#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division
import sys
import os
import codecs
import csv
import argparse
from itertools import islice
import gspread
from gspread.models import Cell

# import pygsheets as gspread
from collections import defaultdict, Counter
from oauth2client.service_account import ServiceAccountCredentials


scope = [
    "https://spreadsheets.google.com/feeds",
    "https://www.googleapis.com/auth/drive",
]
credentials = ServiceAccountCredentials.from_json_keyfile_name(
    "Studchr-9a61ec5b422c.json", scope
)
gc = gspread.authorize(credentials)

recaps_dir = "/Users/pecheny/studchr_ratings_v3/Составы/"
results_dir = "/Users/pecheny/studchr_ratings_v3/ОВСЧ/"
ovsch_ids = ["5818", "5819"]
google_sheet_id = "1BvbvLfz31_Tv3ZuO-QwHEGETq7Dp7g5xYpofaEHNELM"


class InfiniteDict(defaultdict):
    def __init__(self):
        defaultdict.__init__(self, self.__class__)


def tabulate(*args):
    return "\t".join(map(format, args))


results = InfiniteDict()
recaps = InfiniteDict()

DQ = []

ntours = 0


def get_reader(csvfile):
    return csv.reader(csvfile, delimiter=",", quotechar='"')


def get_recaps():
    global recaps
    recaps = defaultdict(lambda: defaultdict(set))
    cwd = os.getcwd()
    os.chdir(recaps_dir)
    i = 0
    for x in sorted(os.listdir(os.getcwd())):
        if x.endswith(".csv") and x.split(".")[-2].split("-")[-1] in ovsch_ids:
            i += 1
            with codecs.open(x, "r", "utf8") as f:
                for sp in islice(get_reader(f), 1, None):
                    if i not in recaps[int(sp[1])]:
                        recaps[int(sp[1])][i] = set()
                    recaps[int(sp[1])][i].add(int(sp[5]))
    os.chdir(cwd)
    return i, recaps


def get_right_one(lst):
    cntr = Counter(lst)
    most = [x for x in cntr if cntr[x] == max(cntr.values())]
    if len(most) == 1:
        return most[0]
    else:
        for x in lst[::-1]:
            if x in most:
                return x


def merge_recaps(recaps_, num):
    result = set()
    for x in range(1, num + 1):
        result = result | recaps_.get(x, set())
    return result


def write_places(teams, points_key="questions"):
    pointslist = [team[points_key] for team in teams]
    for team in teams:
        team["place"] = [
            pointslist.index(team[points_key]) + 1,
            len(pointslist) - pointslist[::-1].index(team[points_key]),
        ]


def parse_results(removals):
    results = defaultdict(lambda: defaultdict(dict))
    names = defaultdict(list)
    towns = defaultdict(list)
    cwd = os.getcwd()
    os.chdir(results_dir)
    i = 0
    for x in sorted(os.listdir(os.getcwd())):
        if x.endswith(".csv"):
            tourn_id = ovsch_ids[i]
            i += 1
            with codecs.open(x, "r", "utf8") as f:
                for sp in islice(get_reader(f), 1, None):
                    team = int(sp[0])
                    if team in removals[tourn_id]:
                        continue
                    names[team].append(sp[1])
                    towns[team].append(sp[2])
                    questions = int(sp[-2])
                    flags = set(sp[-1].split()) | {"all"}
                    team_recaps = merge_recaps(recaps[team], i)
                    if len(team_recaps) > 9:
                        questions = "X"  # mark tour as disqualified
                        DQ.append(
                            (
                                team,
                                i,
                                list(
                                    recaps[team][i]
                                    - merge_recaps(recaps[team], i - 1)
                                ),
                            )
                        )
                        recaps[team][i] = set()
                    for f in flags:
                        results[f][team][i] = questions
    os.chdir(cwd)
    return results, names, towns


class DebugWriter(object):
    def __init__(self, fn):
        self.fn = fn
        self.buffer = ""
        f = codecs.open(self.fn, "w", "utf8")
        f.close()

    def __call__(self, row):
        self.buffer += "\t".join(map(format, row)) + "\n"

    def update(self):
        f = codecs.open(self.fn, "a", "utf8")
        f.write(self.buffer)
        f.close()
        self.buffer = ""


class GoogleSheetWriter(object):
    def __init__(self, worksheet, initrow=1):
        self.ws = worksheet
        self.row = initrow
        self.cells = []

    def __call__(self, lst):
        col = 1
        for el in lst:
            cell = Cell(self.row, col, el)
            self.cells.append(cell)
            col += 1
        self.row += 1

    def update(self):
        self.ws.update_cells(self.cells)
        self.cells = []


def prepare_results(results, names, towns):
    result = defaultdict(lambda: [])
    for flag in results:
        for team in results[flag]:
            gvalues = [
                x for x in results[flag][team].values() if isinstance(x, int)
            ]
            if not gvalues:
                continue
            result[flag].append(
                {
                    "id": team,
                    "name": get_right_one(names[team]),
                    "town": get_right_one(towns[team]),
                    "sum": sum(gvalues),
                    "sum_noworst": sum(gvalues)
                    - (min(gvalues) if len(gvalues) == ntours else 0),
                }
            )
        result[flag].sort(key=lambda x: x["name"])
        result[flag].sort(key=lambda x: x["sum"], reverse=True)
        result[flag].sort(key=lambda x: x["sum_noworst"], reverse=True)
        write_places(result[flag], points_key="sum_noworst")
    return result


def str_place(place, add=0):
    if not isinstance(place[0], int) or not isinstance(place[1], int):
        add = ""
    if place[0] == place[1]:
        try:
            return str(place[0] + add)
        except:
            import pdb

            pdb.set_trace()
    return "{}–{}".format(place[0] + add, place[1] + add)


sheet_dict = {"Общий зачёт": "all", "Студенческий зачёт": "С"}


class WriterWrapper(object):
    def __init__(self, *writers):
        self.writers = writers

    def __call__(self, row):
        for writer in self.writers:
            writer(row)

    def update(self):
        for writer in self.writers:
            writer.update()


def process_sheet(sheet, results, results_prepared, debug=False):
    flag = sheet_dict[sheet.title]
    writers = [DebugWriter("{}.tsv".format(flag))]
    if not debug:
        sheet.clear()
        writers.append(GoogleSheetWriter(sheet))
    writer = WriterWrapper(*writers)
    row = [
        "Место",
        "id",
        "Команда",
        "Город",
        "Сумма без худшего",
        "Сумма",
    ] + list(range(1, ntours + 1))
    writer(row)
    for team in results_prepared[flag]:
        res = [
            results[flag][team["id"]].get(x + 1, "-") for x in range(ntours)
        ]
        row = [
            str_place(team["place"]),
            team["id"],
            team["name"].replace('"', ""),
            team["town"],
            team["sum_noworst"],
            team["sum"],
        ] + res
        writer(row)
    writer.update()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true")
    args = parser.parse_args()
    global ntours
    ntours, recaps = get_recaps()
    removals = defaultdict(set)
    if os.path.isfile("removals.tsv"):
        with codecs.open("removals.tsv", "r", "utf8") as f:
            for line in f:
                tabs = line.split("\t")
                if len(tabs) < 2:
                    continue
                removals[tabs[0]].add(tabs[1])
    results, names, towns = parse_results(removals)

    tours = ["{} этап".format(x + 1) for x in range(ntours)]

    results_prepared = prepare_results(results, names, towns)
    table = gc.open_by_key(google_sheet_id)
    sheets = table.worksheets()
    for sheet in sheets:
        process_sheet(sheet, results, results_prepared, debug=args.debug)

    with codecs.open("recaps.tsv", "w", "utf8") as f:
        f.write(
            tabulate("team_id", "team_name", "players_count", "player_ids")
            + "\n"
        )
        for t in sorted(
            recaps, key=lambda x: len(merge_recaps(recaps[x], 6)), reverse=True
        ):
            f.write(
                tabulate(
                    t,
                    names.get(t, "-"),
                    len(merge_recaps(recaps[t], 6)),
                    ", ".join(map(format, sorted(merge_recaps(recaps[t], 6)))),
                )
                + "\n"
            )


if __name__ == "__main__":
    main()
